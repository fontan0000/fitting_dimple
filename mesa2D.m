%% function
% 2d logistic function for mesa
%function fitmesa(param,x,y,z)

function [p_out] = mesa2D(X,Y,Z)
    % initial, unknown parameters
    tilt_x=0;
    tilt_y=0; % zeroing the tilts, best start possible
    height=3; % usually good guess, fab parameter
    shift=0; %usually a good guess, due to measurement method
    %need better way to find good x0 y0...
    centerx_list=[];
    centery=0; % critical, least known... hopping center is top right
    %wrapping up
    init_param=[tilt_x, tilt_y, height, shift, centerx,centery];
    % define bounds:
    lb=[-2, -2, 1, -1, -40,-40]; % expect low tilt, and mesa can't be out of the field of view
    ub=[2, 2, 5, 5, 140,120]; % expect low tilt, and mesa can't be out of the field of view
    %creating the anon function to be used by lsqcurvefit
    anon_mesa=@(p,xdat) mesafitfun([90,p(3),p(5),p(6),1,p(1),p(2),p(4)],xdat); %mesa size and smoothing are locked
    
    p_out=lsqcurvefit(anon_mesa,init_param,cat(3,X,Y),Z,lb,ub);
    p_out
    
    
%% ancillary functions
    function out = mesafitfun(fitparam,xdata)
        %function getting anonymized for fitting a tilted mesa

        %generate mesa
        trial_mesa=flatmesa2D(fitparam(1:5),{xdata(:,:,1),xdata(:,:,2)});
        %tilt it
        theta_x=fitparam(6);
        theta_y=fitparam(7);
        tilted_trial_mesa=tilt(cat(3,xdata(:,:,1),xdata(:,:,2),trial_mesa),theta_y,theta_x);
        %shift it up/down
        out=tilted_trial_mesa+fitparam(8);
    end

    function [out]=flatmesa2D(param,xy)
        % param are:
        % [mesasize,mesaheight,centerx,centery,smoothing]
        % edge smoothing -> double, larger the steepr
        % center x -> center of the mesa in x um
        % center y -> center of the mesa in y um
        % mesa size -> size of the mesa, default 100um
        % mesaheight -> height of mesa in um

        out = param(2)./(1+exp(-2*param(5)*(param(1)-max(abs(xy{1}-param(3)),abs(xy{2}-param(4))))));
    end


    function tilted_z = tilt(xyz,theta_x,theta_y)
    % rotates by angle theta_x and theta_y around the x, y axis. Small angle
    % approximation: x and y coordinates remain the same, only z gets modified
    % xyz is a 3D matrix of NxMx3 dimension, N and M being the number of pixels
    % in the image

    % rotation vector (not matrix since only affects z):
    R=[-cosd(theta_x).*sind(theta_y), sind(theta_x), cosd(theta_x).*cosd(theta_y)]';

    %unfurl xyz in 2D because matlab doesnt have freakin einsum
    og_size=size(xyz);
    xyz=reshape(xyz,[og_size(1)*og_size(2),og_size(3)]);
    tilted_z=xyz*R;
    % reset the lowest point to zero
    tilted_z=tilted_z-min(tilted_z);
    %reshape the column vector of tilted_z
    tilted_z=reshape(tilted_z,og_size(1:2));

    end

end