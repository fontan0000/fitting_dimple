function [fitresult, zfit, fiterr, zerr, resnorm, rr] = neggaussfit_yf(xx,yy,zz)
% NEGGAUSSFIT Create/alter optimization OPTIONS structure.
%   [fitresult,..., rr] = fmgaussfit(xx,yy,zz) uses ZZ for the surface 
%   height. XX and YY are vectors or matrices defining the x and y 
%   components of a surface. If XX and YY are vectors, length(XX) = n and 
%   length(YY) = m, where [m,n] = size(Z). In this case, the vertices of the
%   surface faces are (XX(j), YY(i), ZZ(i,j)) triples. To create XX and YY 
%   matrices for arbitrary domains, use the meshgrid function. FMGAUSSFIT
%   uses the lsqcurvefit tool, and the OPTIMZATION TOOLBOX. The initial
%   guess for the gaussian is places at the maxima in the ZZ plane. The fit
%   is restricted to be in the span of XX and YY.
%   See:
%       http://en.wikipedia.org/wiki/Gaussian_function
%          
%   Examples:
%     To fit a 2D gaussian:
%       [fitresult, zfit, fiterr, zerr, resnorm, rr] =
%       fmgaussfit(xx,yy,zz);
%   See also SURF, OMPTMSET, LSQCURVEFIT, NLPARCI, NLPREDCI.

%   Copyright 2013, Nathan Orloff.
%   Modified by Natasha Tomm, UniBas 2018. --> fit negative gaussian
%   functions only and optimize to find proper gaussian and not other
%   things.

%   fitresult = fit parameters used to generate the fit.
%         %fitresult=[amp, ang, sx, sy, xo, yo, zo]
%         %amp = Amplitude of Gaussian
%         %ang = Angle between system of coordinates and orientation of
%         gaussian
%         %sx = spread in x
%         %sy = spread in y
%         %xo = location in x of maximum
%         %yo = location in y of maximum
%         %zo = offset of base
%   zfit = the z values of the fit
%   fiterr = error in the fitparameters assuming a confidence interval.
%   zerr = error in the z values assuming the uncertainty in the fit paramteres
%   resnorm = residual
%   rr = reduced chi-squared.

%% Condition the data
%modyf20102021: crop image to avoid edge effect, aggressive cropping
%crop=@(dd) dd(200:end-200,200:end-200);

%xx=crop(xx);
%yy=crop(yy);
%zz=crop(zz);
%mask the data already:
[new_Z_masked, ~] =mask_mesa(zz);
[xData_comp, yData_comp zData_comp] = prepareSurfaceData( xx, yy, zz );
[xData, yData, zData] = prepareSurfaceData( xx, yy, new_Z_masked );
xyData = {xData,yData};


%% Set up the startpoint




pixelsize_y = abs(xx(1,2)-xx(1,1));
pixelsize_x = abs(yy(2,1)-yy(1,1));
zo = median(new_Z_masked(~isnan(new_Z_masked)))-std(new_Z_masked(~isnan(new_Z_masked)));    % offset
xmax = max(xData);
ymax = max(yData);
xmin = min(xData);
ymin = min(yData);
zmax = max(new_Z_masked)*2; % max value of offset
zmin = min(new_Z_masked)/2; % min value of offset
%estimate xo and yo to start search
xo=xmax/2;
yo=ymax/2;

% %prepare image to estimate centroid

%maybe not necessary if masked...
%new_Z_masked=fillplane(zz); % this guy seems to fuck up the filling: the plane is rotate by 90degree

% [row,col]=find(zz==min(min(new_Z_masked)));
% xo=xx(1,col); yo=yy(row,1);


% new_Z_masked(abs(new_Z_masked-zo)>0.5)=zo;
% filtered_zz=abs(new_Z_masked-max(max(new_Z_masked))); %invert image
% filtered_zz(filtered_zz<0.2*max(max(filtered_zz)))=0; %set anything below 20% of max to zero
% % masked_z=fixsharpedges(zz); %mask hard edges
% % masked_z(masked_z<0)=zo; %set hard edges to median value of image
% % filtered_zz=abs(masked_z-max(max(masked_z))); %invert image
% % filtered_zz(filtered_zz<0.2*max(max(filtered_zz)))=0; %set anything below 20% of max to zero
% % filtered_zz=findround(filtered_zz); %select only region which is already kind of round
% %estimate centroid
% [yo,xo]=COG(filtered_zz,yy(:,1),xx(1,:));
% %set initial sx and sy to look around
% filtered_zz(filtered_zz~=0)=1;
% meas=regionprops(filtered_zz,'MajorAxisLength','MinorAxisLength');
% sx=(meas(1).MajorAxisLength)/2.*pixelsize_x;
% sy=(meas(1).MinorAxisLength)/2.*pixelsize_y;
% %set localiton to look for xo,yo
% xmax = xo+10*sx;
% ymax = yo+10*sy;
% xmin = xo-10*sx;
% ymin = yo-10*sy;
%initial amplitude
amp=min(new_Z_masked(:))-max(new_Z_masked(:));
% amp=min(min(zz));

%% Set up fittype and options.
%mod starting points
truth_center = new_Z_masked==min(new_Z_masked(:));
center_xy = [xx(truth_center), yy(truth_center)];
ind_min=round(center_xy./[pixelsize_x, pixelsize_y]);

linex=new_Z_masked(ind_min(2),:);
liney=new_Z_masked(:,ind_min(1));

[~,ind_sigma_xy(1)]=min(abs(linex-(1./exp(1).*abs(zo-min(linex))+min(linex))));
sigma_xy(1) = abs(ind_min(1)-ind_sigma_xy(1))*pixelsize_x;

[~,ind_sigma_xy(2)]=min(abs(liney-(1./exp(1).*abs(zo-min(liney))+min(liney))));
sigma_xy(2) = abs(ind_min(2)-ind_sigma_xy(2))*pixelsize_y;


amp_gauss = -abs(zo-zz(ind_min(2), ind_min(1)));

StartPoint = [amp_gauss, 0, sigma_xy(1), sigma_xy(2), center_xy(1), center_xy(2), zo];

%%

Lower = [-Inf, 0, sigma_xy(1)*0.5, sigma_xy(2)*0.5, center_xy(1)*0.9, center_xy(2)*0.9, zo*0.8];
Upper = [0, 180, sigma_xy(1)*2, sigma_xy(2)*2, center_xy(1)*1.1, center_xy(2)*1.1, zo*1.2]; % angles greater than 90 are redundant
%StartPoint = [amp, ang, pixelsize_x*100, pixelsize_y*100, xo, yo, zo];%[amp, theta, sx, sy, xo, yo, zo];

tols = 1e-16;
options = optimset('Algorithm','trust-region-reflective',...
    'Display','off',...
    'MaxFunEvals',5e3,...
    'MaxIter',5e3,...
    'TolX',tols,...
    'TolFun',tols,...
    'TolCon',tols ,...
    'UseParallel',0);

%zData=new_Z_masked; %added line to use masked data!!!
%[xData, yData, zData] = prepareSurfaceData( xx, yy, zz );  %added line to use masked data!!!

%% perform the fitting
%mmodified yf 07102021
% lambda wrapping function
wrapedgaussian2D =@(par) weighted_gaussian2D(par,xyData,zData);
[fitresult,resnorm,residual] = ...
    lsqnonlin(wrapedgaussian2D,StartPoint,Lower,Upper,options);
[fiterr, zfit, zerr] = gaussian2Duncert(fitresult,residual,xyData);
rr = rsquared(zData, zfit, zerr);
%replaced by placeholder, regardless, not used
zfit =nan;%= reshape(zfit,size(zz));
zerr = nan;%reshape(zerr,size(zz));

end

function rr = rsquared(z,zf,ze)
% reduced chi-squared
dz = z-zf;
rr = 1./(numel(z)-8).*sum(dz.^2./ze.^2); % minus 8 because there are 7 fit parameters +1 (DOF)
end

function z = gaussian2D(par,xy)
% compute 2D gaussian
z = par(7) + ... 
    par(1) * exp(-(((xy{1}-par(5)).*cosd(par(2))+(xy{2}-par(6)).*sind(par(2)))./(sqrt(2)*par(3))).^2-... 
    ((-(xy{1}-par(5)).*sind(par(2))+(xy{2}-par(6)).*cosd(par(2)))./(sqrt(2)*par(4))).^2); 
end

function z = weighted_gaussian2D(par, xy, data)
    % calculates the gaussian, get the raw error deviaption from data,
    % scale that wit signal strength, linearly
    flippeddata = abs(data-max(max(data)));
    maxflipped=max(max(flippeddata));
    cutoff=0.0;
    normdata = (flippeddata/maxflipped+cutoff)/(1+cutoff);
    
    % generate fitted function error vector, rescaled to amplify the dimple
    % signal
    z =  (gaussian2D(par,xy)-data).*normdata;
end

function [dpar,zf,dzf] = gaussian2Duncert(par,resid,xy)
% get the confidence intervals
J = gaussian2DJacobian(par,xy);
parci = nlparci(par,resid,'Jacobian',J);
dpar = (diff(parci,[],2)./2)';
[zf,dzf] = nlpredci(@gaussian2D,xy,par,resid,'Jacobian',J);
end

function J = gaussian2DJacobian(par,xy)
% compute the jacobian
x = xy{1}; y = xy{2};
J(:,1) = exp(- (cosd(par(2)).*(x - par(5)) + sind(par(2)).*(y - par(6))).^2./par(3).^2 - (cosd(par(2)).*(y - par(6)) - sind(par(2)).*(x - par(5))).^2./par(4).^2);
J(:,2) = -par(1).*exp(- (cosd(par(2)).*(x - par(5)) + sind(par(2)).*(y - par(6))).^2./par(3).^2 - (cosd(par(2)).*(y - par(6)) - sind(par(2)).*(x - par(5))).^2./par(4).^2).*((2.*(cosd(par(2)).*(x - par(5)) + sind(par(2)).*(y - par(6))).*(cosd(par(2)).*(y - par(6)) - sind(par(2)).*(x - par(5))))./par(3).^2 - (2.*(cosd(par(2)).*(x - par(5)) + sind(par(2)).*(y - par(6))).*(cosd(par(2)).*(y - par(6)) - sind(par(2)).*(x - par(5))))./par(4).^2);
J(:,3) = (2.*par(1).*exp(- (cosd(par(2)).*(x - par(5)) + sind(par(2)).*(y - par(6))).^2./par(3).^2 - (cosd(par(2)).*(y - par(6)) - sind(par(2)).*(x - par(5))).^2./par(4).^2).*(cosd(par(2)).*(x - par(5)) + sind(par(2)).*(y - par(6))).^2)./par(3)^3;
J(:,4) = (2.*par(1).*exp(- (cosd(par(2)).*(x - par(5)) + sind(par(2)).*(y - par(6))).^2./par(3).^2 - (cosd(par(2)).*(y - par(6)) - sind(par(2)).*(x - par(5))).^2./par(4).^2).*(cosd(par(2)).*(y - par(6)) - sind(par(2)).*(x - par(5))).^2)./par(4)^3;
J(:,5) = par(1).*exp(- (cosd(par(2)).*(x - par(5)) + sind(par(2)).*(y - par(6))).^2./par(3).^2 - (cosd(par(2)).*(y - par(6)) - sind(par(2)).*(x - par(5))).^2./par(4).^2).*((2.*cosd(par(2)).*(cosd(par(2)).*(x - par(5)) + sind(par(2)).*(y - par(6))))./par(3).^2 - (2.*sind(par(2)).*(cosd(par(2)).*(y - par(6)) - sind(par(2)).*(x - par(5))))./par(4).^2);
J(:,6) = par(1).*exp(- (cosd(par(2)).*(x - par(5)) + sind(par(2)).*(y - par(6))).^2./par(3).^2 - (cosd(par(2)).*(y - par(6)) - sind(par(2)).*(x - par(5))).^2./par(4).^2).*((2.*cosd(par(2)).*(cosd(par(2)).*(y - par(6)) - sind(par(2)).*(x - par(5))))./par(4).^2 + (2.*sind(par(2)).*(cosd(par(2)).*(x - par(5)) + sind(par(2)).*(y - par(6))))./par(3).^2);
J(:,7) = ones(size(x));
end