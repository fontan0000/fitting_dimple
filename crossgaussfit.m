function [h,v] = crossgaussfit(X,Y,fitresult,Zdata)
%%{
% Create data cuts along the 
%}
%%
mode_toggle=1; % toggle between reusing the prev fit or using a new one
%%%
imgsize=size(Zdata);
slope=tand(fitresult(2));
if slope < (Y(end,end)-fitresult(6))/(X(end,end)-fitresult(5))
   slope=-slope;
end
    
%getting the masked off mesa
[~,mask]=mask_mesa(Zdata);

%% vertical
%{
slopev=com_slope; %slope of a line
crossingv = fitresult(6)-slopev*fitresult(5); %crossing on the y axis
yv=Y(:,1); %generate y values
xv=(yv-crossingv)./slopev; %generate x values
%get rid of values outside image region
yv(xv<min(min(X)))=0; yv(xv>max(max(X)))=0;
yv(yv==0)=[]; yv(yv==0)=[]; %i don't know why I cannot directly delete those values...
xv(xv<min(min(X)))=[]; xv(xv>max(max(X)))=[];

%}
offsetv=fitresult(6)-(fitresult(5)*1/slope);

yv = Y(:,1);
xv = (yv-offsetv)*(slope);
%trim:
tokick=(xv<X(1,1)|xv>X(1,end));
yv(tokick)=[];
xv(tokick)=[];

%fit gaussian and calculate length of line
zv=gaussian2D(fitresult,{xv,yv}); %z fit values
lv=sign((yv-fitresult(6))).*sqrt((yv-fitresult(6)).^2+(xv-fitresult(5)).^2); %length of v line = sqrt(x^2+y^2);
%get values of z along calculated line
% replaced by interp2
zv_data=interp2(X,Y,Zdata,xv,yv);
%vector containing all info
v={xv,yv,zv,zv_data,lv};
%%


%% horizontal
%{
slopeh=-slope; % slope of a line
crossingh=fitresult(5)-slopeh*fitresult(6); %crossing of the x axis
xh=X(1,:)'; %generate x values
yh=(xh-crossingh)./slopeh; %generate y values
%get rid of values outside image region
xh(yh<min(min(Y)))=0; xh(yh>max(max(Y)))=0;
xh(xh==0)=[]; xh(xh==0)=[]; %i don't know why I cannot directly delete those values...
yh(yh<min(min(Y)))=[]; yh(yh>max(max(Y)))=[];
%}
offseth=fitresult(6)-(fitresult(5)*-1*slope);
xh = X(1,:);
yh = -slope*xh+offseth;
%trim:
tokick=(yv<Y(1,1)|yv>Y(end,1));
yh(tokick)=[];
xh(tokick)=[];


%fit gaussian and calculate length of line
zh=gaussian2D(fitresult,{xh,yh}); %fit gaussian to xy values
lh=sign((xh-fitresult(5))).*sqrt((xh-fitresult(5)).^2+(yh-fitresult(6)).^2); %length of h line = sqrt(x^2+y^2)
%get values of z along calculated line
% replaced by interp2
zh_data=interp2(X,Y,Zdata,xh,yh);
%vector containing all info
h={xh,yh,zh,zh_data,lh};


                
end

function z = gaussian2D(par,xy)
% compute 2D gaussian
z = par(7) + ... 
    par(1) * exp(-(((xy{1}-par(5)).*cosd(par(2))+(xy{2}-par(6)).*sind(par(2)))./(sqrt(2)*par(3))).^2-... 
    ((-(xy{1}-par(5)).*sind(par(2))+(xy{2}-par(6)).*cosd(par(2)))./(sqrt(2)*par(4))).^2); 
end