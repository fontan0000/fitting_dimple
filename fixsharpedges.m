function masked_z=fixsharpedges(zz,threshold)
%this function gets an image zz and fix flaws in the image (such as sharp interfaces)
%by detecting hard edges and masking it

    e=edge(zz,'roberts',threshold,'nothinning'); %find sharp edges
    BW=bwareaopen(e,10); %remove smaller than 4 pixels areas
    cc = bwconncomp(BW); %find connected components in binary image
    %% connect and close points/holes 
    
    BWblank = false(cc.ImageSize);
    stats = regionprops(cc,'ConvexImage','EulerNumber');
    for i = find([stats.EulerNumber]>0)
        distIm = bwdist(~stats(i).ConvexImage);
        maxClose = ceil(max(distIm(:)));
        BWslice = BWblank;
        BWslice(cc.PixelIdxList{i}) = true;
        if isinf(maxClose), continue; end
        for dilSz = 2:maxClose
            BWnew = imdilate(BWslice,ones(dilSz));
            statsNew = regionprops(BWnew,'EulerNumber');
            if statsNew.EulerNumber<=0
                BWnew = imerode(imfill(BWnew,'holes'),ones(dilSz));
                cc.PixelIdxList{i} = find(BWnew);
            end
        end
    end
    
%     % convex hull to close more
%     stats = regionprops(cc,'ConvexImage','EulerNumber','BoundingBox');
%     for i = find([stats.EulerNumber]>0)
%         maxClose = ceil(max(distIm(:)));
%         BWslice = BWblank;
%         BWslice(cc.PixelIdxList{i}) = true;
%         distIm = bwdist(~BWslice);
%         if ~any(distIm(:)>1)
%             BWnew = BWslice;
%             bb = ceil(stats(i).BoundingBox);
%             BWnew((1:bb(4))+bb(2)-1,(1:bb(3))+bb(1)-1) = stats(i).ConvexImage;
%             cc.PixelIdxList{i} = find(BWnew);
%         end
%     end
%     L = imfill(labelmatrix(cc),'holes');
%     % Now we know that any blobs surrounded by other blobs are actually holes
%     indsOfHoles = find(arrayfun(@(i)mode(double(L(bwmorph(L==i,'dilate',1)&~(L==i)))),1:cc.NumObjects));
%     L(ismember(L,indsOfHoles)) = 0;
    
    %close further
    L = imfill(labelmatrix(cc),'holes');
    L=bwmorph(L,'bridge');
    L=bwmorph(L,'close');
    L=bwconvhull(L);
    L=imdilate(L,ones(5,5));
    masked_z=zz;
    masked_z(L)=-1;
   
end