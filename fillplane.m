function new_Z_masked = fillplane(z)
%this function receives a height profile Z,
%find hard edges and fills the imperfections with a fitted plane
%coordinate, given by the Normal Vector of a plane n=(a,b,c);

                %mask it if there are sharp edges
                %masked_z=fixsharpedges(z,0.015);
                masked_z = mask_mesa(z);
                Nx=size(masked_z,2); %number of x pixels
                Ny=size(masked_z,1); %number of y pixels
                masked_z(masked_z<0)=NaN;
                
                % new_Z_masked: substitute all bad points by an artificial plane
                %refit a plane, find normal and substitute bad points
                tiltXYZ=[];
                %downsample to fit plane easier
                for ii=1:10:Ny
                    for jj=1:10:Nx
                        if isnan(masked_z(ii,jj))
                            continue
                        else
                            tiltXYZ=[tiltXYZ; ii jj z(ii,jj)];
                        end
                    end
                end
            
                %fit plane to tiltXYZ coordinates
                [n,~,~] = affine_fit(tiltXYZ); %fit plane to tiltXYZ data to determine x and y angle
                normal=n'; %normal vector of plane
                %normal([1,2,3])=normal([2,1,3]);
                p=tiltXYZ(fix(end/4),:); %a point that belongs to the plane
                
                %find coefficients of plane equation
                A=normal(1); B=normal(2); C=normal(3);
                D=-(normal(1)*p(1)+normal(2)*p(2)+normal(3)*p(3));
                
                %for all bad points substitute by equivalent value in plane
                %Ax+By+Cz+D=0;
                new_Z_masked=zeros(Ny,Nx);
                for aa=1:Ny
                    for bb=1:Nx
                        if isnan(masked_z(aa,bb)) %if bad point
                            z_point=(-D-A*bb-B*aa)/C;
                            new_Z_masked(aa,bb)=z_point;
                        else
                            new_Z_masked(aa,bb)=z(aa,bb);
                        end
                    end
                end
                
end