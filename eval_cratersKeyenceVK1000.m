function summary_res=eval_cratersKeyence(dirName)

% This function receives a directory containing *.csv files generated by VK
% Analyzer. This file should contain height information about a cavity
% fabricated with laser ablation.
% This function then corrects possible tilt
% in the image and calculates the height of the crater and the fitted
% radius of curvature.

    %% Searching for files in the directory
    fileName=getAllFiles(dirName); %lists all (non-directory) files in directory
    fileListsize=length(fileName); %gets size of list
    %mod yf 2021, rid of unnecessary files:
    for ifile=fileListsize:-1:1
        if ~strcmp('csv', fileName{ifile}(end-2:end))
            fileName(ifile)=[];
        end
    end
    fileListsize=length(fileName); %update list size
    
    if fileListsize==0
        error('Directory empty!'); %if folder is empty, emit a warning

    else
        dirNameparts=strsplit(dirName,'\'); %get parts of path
%         samplename=dirNameparts{end}; %get name of sample (name of directory)
        samplename='mesa';
        fileId=fopen(strcat(dirName,'\',samplename,'_craters.txt'),'wt'); %wt is permission to write text
        fprintf(fileId,'Crater\tRadius [um]\tDepth [um]\tWidth [um]\tAsymmetry\ttheta [deg]\n');
        fclose(fileId);
        % summary fit results:
        summary_res = nan(fileListsize,5); %get numerical values obtain from fits
        for ii=1:fileListsize
            [~,name,ext]=fileparts(fileName{ii}); %get name of path, file, extension
            
            %initializing variables
            height=[nan]; height_err=[nan];
            radius1=[nan]; radius2=[nan]; radius=[nan];
            width=[nan]; width_err=[nan];
            theta=[nan]; asymmetry=[nan];
            
           
            if strcmp(ext,'.csv') %reads only csv files
         %%     %acquire data on height of the crater
                Nx=xlsread(fileName{ii},'B9:B9'); %read number of x pixels
                Ny=xlsread(fileName{ii},'B10:B10'); %read number of y pixels

                xyCal=xlsread(fileName{ii},'B7:B7'); %um/pixel

                rowcol_name=[char(base2dec(dec2base(Nx,26)',26)+64)' num2str(Ny+15)];
                rowcol_read=['A16:',rowcol_name];

                z=xlsread(fileName{ii},rowcol_read); %get data only on range where info about height is %transform in micrometers

                fig1=figure('units','pixels','outerposition',[1 1 1200 975]);
        %%    %correct the tilt --> equation of a plane ax+by+cz=d;
                try
                    new_Z=tiltcorrect(z);
                catch
                    warning('l.58: failed to correct tilt')
                    new_Z=z;
                end
               % normalize
               %new_Z=(new_Z-min(new_Z(:)))./new_Z-min(new_Z(:));
        %%    %3D analysis
                y=xyCal*linspace(1,Ny,Ny)'; x=xyCal*linspace(1,Nx,Nx)'; %x and y in microns
                [X,Y]=meshgrid(x,y);

                try
            %    %fit 2D gaussian
                    %[fitresult, zfit, fiterr, zerr, resnorm, rr]=neggaussfit(X,Y,new_Z);
                    [fitresult, zfit, fiterr, zerr, resnorm, rr]=neggaussfit_yf(X,Y,new_Z);
                    pause(0.5)
                    %calculate height from gaussian fit taking into account the difference from gaussian fit to actual data
                    [x0,x0_index]=min(abs(fitresult(5)-x));
                    [y0,y0_index]=min(abs(fitresult(6)-y));
                    if abs(fitresult(1))<=fitresult(7)
                        height=abs(abs(fitresult(1))-new_Z(y0_index,x0_index)+(fitresult(7)-abs(fitresult(1))));
                        height_err=abs(fiterr(1)+y0+x0);
                    elseif abs(fitresult(1))>fitresult(7)
                        height=abs(fitresult(7)-new_Z(y0_index,x0_index));
                        height_err=abs(fiterr(1)+y0+x0);
                    end
                    %get angle
                    theta=fitresult(2);

            %    %finding principal axis from gaussians in the two directions according to fit
                    [h,v]=crossgaussfit(X,Y,fitresult,new_Z);
                    pause(0.5)
                    try
                    %plot cropped down data:              
                    [~,ind_lh_bound(1)]=min(abs(h{1}-fitresult(5)+4*mean(fitresult(3:4))));
                    [~,ind_lh_bound(2)]=min(abs(h{1}-fitresult(5)-4*mean(fitresult(3:4))));
                    [~,ind_lv_bound(1)]=min(abs(v{2}-fitresult(6)+4*mean(fitresult(3:4))));
                    [~,ind_lv_bound(2)]=min(abs(v{2}-fitresult(6)-4*mean(fitresult(3:4))));
                        
                %    %fit circle to data on principal and non-principal axes
                     %get peak horizontal
                        [~,~,wh,~]=findpeaks(-h{4}(ind_lh_bound(1):ind_lh_bound(2)),h{5}(ind_lh_bound(1):ind_lh_bound(2)),...
                            'MinPeakProminence',0.5*abs(fitresult(1)));
                        [~,locminh,widthminh,~]=findpeaks(-h{4}(ind_lh_bound(1):ind_lh_bound(2)),...
                            'MinPeakProminence',0.5*abs(fitresult(1)));
                        locminh=locminh+ind_lh_bound(1);
                     %get peak vertical
                        [~,~,wv,~]=findpeaks(-v{4}(ind_lv_bound(1):ind_lv_bound(2)),v{5}(ind_lv_bound(1):ind_lv_bound(2)),...
                            'MinPeakProminence',0.5*abs(fitresult(1)));
                        [~,locminv,widthminv,~]=findpeaks(-v{4}(ind_lv_bound(1):ind_lv_bound(2)),...
                            'MinPeakProminence',0.5*abs(fitresult(1)));
                        locminv=locminv+ind_lv_bound(1);
                     %calculate width of crater
                        width=mean([wh,wv]);
                        width_err=std([wh,wv]);
                    catch
                        warning('Failed to find peaks');
                        width=[];
                        width_err=[];
                    end
                    try 
                        %fit circle horizontal
                        circlefitpointsh=(floor(locminh-widthminh/1.85):floor(locminh+widthminh/1.85))'; %consider only points from peak to 1/e of the peak
                        circleparh=circlefit(h{5}(circlefitpointsh),h{4}(circlefitpointsh)); %fit circle to points
                        circlevech=circle(circleparh); %generate circle from fit to plot

                        %fit circle vertical
                        circlefitpointsv=(floor(locminv-widthminv/1.85):floor(locminv+widthminv/1.85))'; %consider only points from peak to 1/e of the peak
                        circleparv=circlefit(v{5}(circlefitpointsv),v{4}(circlefitpointsv));
                        circlevecv=circle(circleparv);

                        %calculate mean radius
                        radius1=circleparv(3);
                        radius2=circleparh(3);
                        radius=mean([radius1,radius2]);
                        %calculate asymmetry
                        asymmetry=abs(circleparv(3)-circleparh(3))/(circleparv(3)+circleparh(3));
                    catch
                        warning('Failed to fit circles');
                        radius=[];
                        radius1=[];
                        radius2=[];
                        asymmetry=[];
                    end
                    
                    
                %%  %plotting
                % std matlab color
                mat_blue=[0,0.447,0.741];
                mat_orange=[0.929,0.694,0.125];
                mat_maroon=[0.635,0.078,0.184];
                mat_gray=[0.2,0.2,0.2];
                
                try
                    %plot cropped down data:
                                    
                    [~,ind_x_bound(1)]=min(abs(h{1}-fitresult(5)+4*mean(fitresult(3:4))));
                    [~,ind_x_bound(2)]=min(abs(h{1}-fitresult(5)-4*mean(fitresult(3:4))));
                    [~,ind_y_bound(1)]=min(abs(v{2}-fitresult(6)+4*mean(fitresult(3:4))));
                    [~,ind_y_bound(2)]=min(abs(v{2}-fitresult(6)-4*mean(fitresult(3:4))));
                    
                    %%horizontal
                    ax2=subplot(3,4,[1,2,3]);
                    ind_h_bound=ind_x_bound;                    
                    plot(h{5}(ind_h_bound(1):ind_h_bound(2)),h{4}(ind_h_bound(1):ind_h_bound(2)),...
                        'o','MarkerEdgeColor','k','MarkerFaceColor','none','Markersize',3,'LineWidth',1.2); %plot measurement points
                    hold on
                    plot(h{5}(ind_h_bound(1):ind_h_bound(2)),h{3}(ind_h_bound(1):ind_h_bound(2)),'-','color',mat_orange,'Linewidth',1.5); %plot gaussian fit
                    plot(circlevech(:,1),circlevech(:,2),'b','color',mat_blue,'Linewidth',1.5);
                    xlim(h{5}(ind_h_bound));
                    minh=min(h{4}(ind_h_bound(1):ind_h_bound(2)))-0.1; maxh=max(h{4}(ind_h_bound(1):ind_h_bound(2)))*1.1;
                    ylim([minh maxh]);
                    xlabel('{\itlength [\mum]}'); ylabel('{\itz [\mum]}');
                    legend('Data points','Gaussian fit','Circular fit');

                    %vertical
                    ax3=subplot(3,4,[8,12]);
                    ind_v_bound=ind_y_bound;
                    
                    plot(v{4}(ind_v_bound(1):ind_v_bound(2)),v{5}(ind_v_bound(1):ind_v_bound(2)),...
                        'o','MarkerEdgeColor','k','MarkerFaceColor','none','Markersize',3,'LineWidth',1.2); %plot measurement points
                    hold on
                    plot(v{3}(ind_v_bound(1):ind_v_bound(2)),v{5}(ind_v_bound(1):ind_v_bound(2)),'-','color',mat_maroon,'Linewidth',1.5);
                    plot(circlevecv(:,2),circlevecv(:,1),'b','color',mat_blue,'Linewidth',1.5);
                    minv=min(v{4}(ind_v_bound(1):ind_v_bound(2)))-0.1; maxv=max(v{4}(ind_v_bound(1):ind_v_bound(2))).*1.1;
                    xlim([minv maxv]);
                    ylim(v{5}(ind_v_bound));
                    set(gca,'YDir','reverse');
                    xlabel('{\itz [\mum]}'); ylabel('{\itlength [\mum]}');
                    set(gca,'YDir','reverse');
                    legend('Data points','Gaussian fit','Circular fit');

                catch
                    warning('l.145-181: failed to plot main panel')
                end
                
                catch
                    warning('Failed to fit  to data');
                    height=[];
                    theta=[];
                end
                    

                %main axis
                ax1=subplot(3,4,[5,6,7,9,10,11]);
                try
                imagesc(x(ind_x_bound(1):ind_x_bound(2)),y(ind_y_bound(1):ind_y_bound(2)),...
                    new_Z(ind_y_bound(1):ind_y_bound(2),ind_x_bound(1):ind_x_bound(2)));
                xlabel('{\itx [\mum]}'); ylabel('{\ity [\mum]}');
                cmap=1-((1-[0.2,0.2,0.6].*ones(256,3)).*(1-0.9.*abs(sin(2*pi/0.637*linspace(0,max(max(new_Z(ind_y_bound(1):ind_y_bound(2),ind_x_bound(1):ind_x_bound(2)))),256))))');
                colormap(gcf,cmap);
                clrbar=colorbar;
                clrbar.Label.String='{\it[\mum]}';
                axis tight;
                axis image;
                catch
                    warning("error line 182-192 evalKeyence")
                end
                try
                    hold on
                    plot(h{1}(ind_h_bound(1):ind_h_bound(2)),h{2}(ind_h_bound(1):ind_h_bound(2)),'-','color',mat_orange,'Linewidth',1.5); %horizontal line
                    plot(v{1}(ind_v_bound(1):ind_v_bound(2)),v{2}(ind_v_bound(1):ind_v_bound(2)),'-','color',mat_maroon,'Linewidth',1.5); %vertical line
                    hold off
                catch
                end
                
                try
                %text
                ax4=subplot(3,4,4);
                %calculate other parameters and print
                infotext{1}=['Height: ',strcat(' ',num2str(height,'%.3f'),'\pm',num2str(height_err,'%.3f'),' \mum')];
                infotext{2}=['FWHM: ',strcat(' ',num2str(width,'%.3f'),'\pm',num2str(width_err,'%.3f'),' \mum')];
                infotext{3}=['Radius 1: ',strcat(' ',num2str(radius1,'%.3f')),' \mum'];
                infotext{4}=['Radius 2: ',strcat(' ',num2str(radius2,'%.3f')),' \mum'];
                infotext{5}=['Asymmetry: ',strcat(' ',num2str(asymmetry,'%.3f'))];
                infotext{6}=['\theta: ',strcat(' ',num2str(theta,'%.2f')),'^{\circ}'];
                text(0.1,0.85,name,'Interpreter','none');
                text(0.1,0.70,infotext{1},'Interpreter','tex');
                text(0.1,0.60,infotext{2},'Interpreter','tex');
                text(0.1,0.50,infotext{3},'Interpreter','tex');
                text(0.1,0.40,infotext{4},'Interpreter','tex');
                text(0.1,0.30,infotext{5},'Interpreter','tex');
                text(0.1,0.20,infotext{6});
                set(gca,'visible','off');
                catch
                    warning('l.217-232: failed to plot the fitted values')
                end
                
                try
                    %make axes with equal sizes
                    ax1Pos = get(ax1,'position'); %get size main axis
                    ax2Pos = get(ax2,'position'); %get size horizontal axis
                    ax3Pos = get(ax3,'position'); %get size vertical axis
                    %correct position of horizontal axis
                    ax2Pos(3) = ax1Pos(3);
                    set(ax2,'position',ax2Pos);
                    %correct position of horizontal axis
                    ax3Pos(4) = ax1Pos(4);
                    set(ax3,'position',ax3Pos);
                catch
                    warning('l.234-243: failed to correct axis positions')
                end

                %% saving figure
                saveas(fig1,strcat(dirName,'\',name,'.png'));

                %% keeping info to create a log file
                craterNameparts=strsplit(name,'_'); %get parts of path
                cratername=craterNameparts{end}; %get name of sample (name of directory)
                fileId=fopen(strcat(dirName,'\',samplename,'_craters.txt'),'a'); %wt is permission to write text
                fprintf(fileId,strcat(cratername,'\t',num2str(radius),'\t',num2str(height),'\t',num2str(width),'\t',num2str(asymmetry),'\t',num2str(theta),'\n'));
                fclose(fileId);
                
<<<<<<< HEAD
=======
                % fill the summary array:
                %summary_res(ii,:)=[radius,height,width,asymmetry,theta];
>>>>>>> mod_work
                clearvars -except fileName samplename fileListsize dirName fileId nameparts
            end
        end
        
        
    end


end


