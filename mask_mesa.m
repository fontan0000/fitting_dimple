function [zz_masked, mask]=mask_mesa(zz)
%yf,20.10.2021
% provides a mask for the dataset zz with bit1 values (hopefuly) only on
% the mesa

% generate an height histogram
hist.BinCounts=[];
hist.BinEdges=[];
hist.BinWidth=[];
[hist.BinCounts, hist.BinEdges]=histcounts(zz,100);
hist.BinWidth=hist.BinEdges(2)-hist.BinEdges(1);
%close(gcf);
% threshold to binarize positive the most represented elements, likely the
% mesa plateau, taking 3 binsize into account
% since somethimes the lower level can give the highest bin, lets not take
% the histogram from the very bottom, say 200nm
bincutoff=round(0.2/hist.BinWidth)+1; % + 1 for indexing
bittedzz=bitand(zz>hist.BinEdges(find(hist.BinCounts==max(hist.BinCounts(bincutoff:end))))-3*hist.BinWidth,...
                zz<hist.BinEdges(find(hist.BinCounts==max(hist.BinCounts(bincutoff:end))))+3*hist.BinWidth);
%fill the holes in the bitted image:
bittedfilledzz = imfill(bittedzz,'holes');

%finding bounding boxes, give a long list
stat = regionprops(bittedfilledzz,'boundingbox');
% trying to grade the boxes and find the most likely
bbox_array=reshape(struct2array(stat),4,numel(stat))'; % transfer structure to n by 4 array

%based on ratio
ratio=bbox_array(:,3)./bbox_array(:,4);
ratio_accept=bitand(ratio>0.5,ratio<2);
% reduce the list
bbox_array(~ratio_accept,:)=[];
%based on area, anything less than a 100square pixel
area=bbox_array(:,3).*bbox_array(:,4);
bbox_array(area<100,:)=[];
%based on flatness
flatness=nan(size(bbox_array,1),1);
median_height=nan(size(bbox_array,1),1);
includecenter=[];
for j=1:size(bbox_array,1)
    xbl=round(bbox_array(j,1))+1;
    xbr=round(bbox_array(j,1)+bbox_array(j,3))-1;
    
    ybd=round(bbox_array(j,2))+1;
    ybu=round(bbox_array(j,2)+bbox_array(j,4))-1;

    box_data=zz(ybd:ybu,xbl:xbr);
    flatness(j)=std(reshape(box_data,1,numel(box_data)));
    median_height(j)=median(reshape(box_data,1,numel(box_data)));
    % include zz x center:
    yok=(bbox_array(j,1)<size(zz,1)./2 && (bbox_array(j,1)+bbox_array(j,3))>size(zz,1)./2);
    xok=(bbox_array(j,2)<size(zz,2)./2 && (bbox_array(j,2)+bbox_array(j,4))>size(zz,2)./2);
    includecenter(j)=yok && xok;
end

% giving grades:
[~,ind_grade_flat]=sort(flatness,'ascend');
grade_flatness(ind_grade_flat)=linspace(1,0,numel(flatness)); % flatness

[~,ind_grade_height]=sort(median_height,'descend');
grade_height(ind_grade_height)=linspace(1,0,numel(median_height)); %median height

[~,ind_grade_area]=sort(bbox_array(:,3).*bbox_array(:,4),'descend'); %area
grade_area(ind_grade_area)=linspace(1,0,numel(median_height)).^2;

total_grade=(grade_flatness+grade_height+grade_area+includecenter); %total grade
[~,ind_winner]=max(total_grade);
% get the winner
bbox_win=bbox_array(ind_winner,:);
mask_win=zeros(size(zz));

xbl=round(bbox_win(1))+1;
xbr=round(bbox_win(1)+bbox_win(3))-1;
ybd=round(bbox_win(2))+1;
ybu=round(bbox_win(2)+bbox_win(4))-1;

mask_win(ybd:ybu,xbl:xbr)=1;
zz_mask=bitand(bittedfilledzz,mask_win);
%extra 10pixel erosion
zz_mask=logical(imerode(zz_mask,strel('square',10)));

zz_masked=zz;
zz_masked(~zz_mask)=nan;
mask=zz_mask;
end