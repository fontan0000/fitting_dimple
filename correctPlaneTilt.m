function [new_Z]=correctPlaneTilt(z,pointsTilt)
    %%    %correct the tilt --> equation of a plane ax+by+cz=d;
                
                Nx=size(z,2); %number of x pixels
                Ny=size(z,1); %number of y pixels
                
                %original image
                pointsTilt_z=[z(pointsTilt(1,1),pointsTilt(1,2));...
                              z(pointsTilt(2,1),pointsTilt(2,2));...
                              z(pointsTilt(3,1),pointsTilt(3,2))]; %get z values of points to fit a plane
                pointsTilt=[pointsTilt,pointsTilt_z]; %concatenate z values to pointsTilt
                normal=cross(pointsTilt(1,:)-pointsTilt(2,:),pointsTilt(1,:)-pointsTilt(3,:)); %find normal of plane
                
                %calculating angle of rotation: we want to keep x and y, and correct z tilt
                artif_normal=[0,0,1]; %artificial normal
                theta=acos(dot(normal,artif_normal)/(norm(normal)*norm(artif_normal))); %angle of rotation
                axis=cross(normal,artif_normal)/norm(cross(normal,artif_normal)); %calculate axis of rotation
                c=cos(theta); s=sin(theta); C=1-c;
                rotMatrix=[axis(1)*axis(1)*C+c, axis(1)*axis(2)*C-axis(3)*s, axis(1)*axis(3)*C+axis(2)*s;...
                           axis(2)*axis(1)*C+axis(3)*s, axis(2)*axis(2)*C+c, axis(2)*axis(3)*C-axis(1)*s;...
                           axis(3)*axis(1)*C-axis(2)*s, axis(3)*axis(2)*C+axis(1)*s, axis(3)*axis(3)*C+c];
                    
                %calculate rotated plane values  
                new_Z=zeros(Ny,Nx);
                for aa=1:Ny
                    for bb=1:Nx
                        newpoint=rotMatrix*[aa;bb;z(aa,bb)];
                        new_Z(aa,bb)=newpoint(3); %adjust only height (z) value
                    end
                end
                
end