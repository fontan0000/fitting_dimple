%%
%{
Script processing the craters, producing graphs etc
%}

[MetaStruct] = summary_allcraters(pathmain);

%%
% first some raw numbers:
total_chip=numel(unique(string(ExtractField(MetaStruct,'name'))));
disp(['Total number of chip folder found: ',num2str(total_chip)])

% number of successfully fitted etc (with reasonable parameters)
filtstruct = filterStrArr(MetaStruct,'nofile',0,'nodata',0,'failedfit',0,'radius',[5,35],'width',[3,10]);
success_chips_list=unique(string(ExtractField(filtstruct,'name')));
success_chips=numel(success_chips_list);
total_success = numel(filtstruct);

disp(['Total number of chips with successful fits: ',num2str(success_chips)])
disp(['Total number of fits: ',num2str(total_success)])

% squeezing the time data:
timearray_all=dateshift([min(datetime(cell2mat(ExtractField(MetaStruct,'date')))):max(datetime(cell2mat(ExtractField(MetaStruct,'date'))))],'start','day');
timearray_filt=dateshift([min(datetime(cell2mat(ExtractField(filtstruct,'date')))):max(datetime(cell2mat(ExtractField(filtstruct,'date'))))],'start','day');

% plots
position=[2.1466,1.4786,0.9232,0.4640]*1e3;
%%
%histograms
fig_hist=figure;
%radius
subplot(2,2,1)
histogram(cell2mat(ExtractField(filtstruct,'radius')))
xlabel('radius (um)');ylabel('number of crater')
% width
subplot(2,2,3)
histogram(cell2mat(ExtractField(filtstruct,'width')))
xlabel('width (um)');ylabel('number of crater')
%depth
subplot(2,2,2)
histogram(cell2mat(ExtractField(filtstruct,'depth')))
xlabel('depth (um)');ylabel('number of crater')
%asym
subplot(2,2,4)
histogram(cell2mat(ExtractField(filtstruct,'asymmetry')))
xlabel('asymmetry (um)');ylabel('number of crater')

sgtitle(['Dimple histograms -- ',date])
set(gcf,'Position',position)
saveas(gcf,[pathmain,'\histograms_',date,'.png'])
%%
%relations
fig_rel=figure;
%radius:depth
subplot(2,2,1)
plot(cell2mat(ExtractField(filtstruct,'radius')),cell2mat(ExtractField(filtstruct,'depth')),...
    'ok','MarkerFaceColor','k','MarkerSize',2)
xlabel('radius (um)');ylabel('depth (um)')
%radius:width
subplot(2,2,2)
plot(cell2mat(ExtractField(filtstruct,'radius')),cell2mat(ExtractField(filtstruct,'width')),...
    'ok','MarkerFaceColor','k','MarkerSize',2)
xlabel('radius (um)');ylabel('width (um)')
%radius:asymmetry
subplot(2,2,3)
plot(cell2mat(ExtractField(filtstruct,'radius')),cell2mat(ExtractField(filtstruct,'asymmetry')),...
    'ok','MarkerFaceColor','k','MarkerSize',2)
xlabel('radius (um)');ylabel('asymmetry')
%radius:asymmetry
subplot(2,2,4)
plot(cell2mat(ExtractField(filtstruct,'depth')),cell2mat(ExtractField(filtstruct,'asymmetry')),...
    'ok','MarkerFaceColor','k','MarkerSize',2)
xlabel('depth (um)');ylabel('asymmetry')

sgtitle(['Dimple correlation -- ',date]) 
set(gcf,'Position',position)
saveas(gcf,[pathmain,'\fitparameters_',date,'.png'])
%%
% time
fig_time=figure;
% radius
subplot(2,1,1)
plot(datetime(cell2mat(ExtractField(filtstruct,'date'))),cell2mat(ExtractField(filtstruct,'radius')),...
    'ok','MarkerFaceColor','k','MarkerSize',3)
xlabel('day');ylabel('radius (um)')
%asymmetry
subplot(2,1,2)
plot(datetime(cell2mat(ExtractField(filtstruct,'date'))),cell2mat(ExtractField(filtstruct,'asymmetry')),...
    'ok','MarkerFaceColor','k','MarkerSize',3)
xlabel('day');ylabel('asymmetry (um)')

sgtitle(['Dimple time evolution -- ',date])
set(gcf,'Position',position)
saveas(gcf,[pathmain,'\timeline_',date,'.png'])


%Chip based data
success_chips_rad=nan(success_chips,2);
success_chips_depth=nan(success_chips,2);
success_chips_asym=nan(success_chips,2);
for i=1:success_chips
    findInStrArr(filtstruct,'name',success_chips_list(success_chips),'radius')
end



%%


%% main gathering function
    function [MetaStruct]=summary_allcraters(pathmain)
        % fetches all the fitted infor from all chips and mesa and summarize them

        %get everything in the batch folder
        list_all = dir(pathmain);
        elements_N = size(list_all); % number f elements
        % generate a cleaned up list:
        % start with a folder mask
        mask_folders=zeros(elements_N)-1;
        for i=1:elements_N
            name_el = list_all(i).name;
            if numel(name_el)<5
                continue % that's for sure
            elseif (strcmp(name_el(1:2),'co') || strcmp(name_el(1:2),'ce')) && all(isstrprop(name_el(end-2:end),'digit'))
                mask_folders(i)=i;
            end     
        end
        % remove non-chip folders
        mask_folders(mask_folders==-1)=[];
        % generate chip list, clean up the fields to keep name, folder and
        % creation dates
        list_chip = list_all(mask_folders);
        list_chip = rmfield(list_chip,'bytes');
        list_chip = rmfield(list_chip,'isdir');
        list_chip = rmfield(list_chip,'datenum');

        %Now, same idea, same structure, but adding the field corresponding to
        %the mesa so we reach a maximum of 10*number chip folder list

        for chipN=1:numel(list_chip)
            disp(list_chip(chipN).name)
            if strcmp(list_chip(chipN).name,'co_f_050')
                continue
            end
            if chipN==1
                MetaStruct = CreateMesaStruct(list_chip(chipN));
            else 
                MetaStruct = [MetaStruct; CreateMesaStruct(list_chip(chipN))];
            end
        end 
    end
    %%
    % Adjunct functions:
    function StructArr = CreateMesaStruct(chipstruct)
            % goes to dirName, fetch the crater summary and returns an array of
            % struct. Always return a struct, mark if there was no fitting
            chipdirname = chipstruct.folder;
            chipstr = chipstruct.name;
            chipdate = chipstruct.date;
            fnm = {'name','mesa','radius','depth','width','asymmetry','angle',...
                   'folder','date','nofile','nodata','failedfit'}; % field names
            mnm = {'x0y0', 'x1y1', 'x1y2', 'x1y3', 'x2y1', 'x2y2', 'x2y3', 'x3y1', 'x3y2', 'x3y3'};
             
            %get summary_fitresult to check and if possible import data
            [flag_datafile, crater_raw, data_arrayed]=...
                summary_fitresult([chipdirname,'\',chipstr],false); % flag'ed false so that it doesnt pop a graph
            % pass char array into cell array of char
            if isempty(crater_raw) && isempty(data_arrayed)
                crater_raw=mnm;
                data_arrayed=nan(10,5);
            else
                crater_raw=cellstr(crater_raw);
            end
            % creeate cell array for data regardless if data or not:
            chipcell=cell(10,12);
            chipcell(:,1)={chipstr}; % distribute the chip name
            chipcell(:,8)={chipdirname}; % distribute the chip folder
            chipcell(:,9)={chipdate}; % distribute the chip date
            % if no data file: create a blanck with appropriate flag for
            % nofile
            if flag_datafile==-2        
                chipcell(:,2) = mnm; % fill mesa names
                chipcell(:,3:7) = {nan}; % nan the data fields
                chipcell(:,10) = {1}; %NO FILE flag, TRUE
                chipcell(:,11) = {0}; %NO data flag, false
                chipcell(:,12) = {0}; %Failed fit flag, false
            elseif flag_datafile==0 % if data, do the following
                chipcell(:,2)=mnm; % fill mesa names
                chipcell(:,10)={0}; %NO FILE flag, FALSE
                for k=1:numel(mnm)
                    %get the return data array index for this crater
                    ind_mes=find(strcmp(crater_raw,mnm{k}));
                    if isempty(ind_mes) % if there is no such mesa in the file
                        chipcell(k,3:7) = {nan}; % nan the data fields
                        chipcell(k,11)={1}; %NO data flag, True
                        chipcell(k,12)={1}; %Failed fit flag, True
                    else % if there is one
                        data_celled = num2cell(data_arrayed(ind_mes,:)); %fill with data, incl. nan
                        chipcell(k,3:7) = data_celled; %fill with data, incl. nan
                        if all(isnan(data_arrayed(ind_mes,:))) %full with nan's
                            chipcell{k,11}=1; %NO data flag, True
                            chipcell{k,12}=1; %Failed fit flag, True
                        elseif all(~isnan(data_arrayed(ind_mes,:)))
                            chipcell{k,11}=0; %NO data flag, FALSE
                            chipcell{k,12}=0; %Failed fit flag, FALSE
                        else % partial failure
                            chipcell{k,11}=0; %NO data flag, FALSE
                            chipcell{k,12}=1; %Failed fit flag, True
                        end
                    end
                    % now the chip array is full no matter what
                end
            end
            % now we have the chip cell and the fieldname fnm, so we can
            % cast as an array of struct with same field:
            for l=numel(mnm):-1:1
                StructArr(l)=cell2struct(chipcell(l,:)',fnm);
            end
            % transpose the array
            StructArr=StructArr';
            % ...and exit
    end
    
    function FieldVals = ExtractField(StrArr,FieldName)
        % extract the entire Field column FieldName of an array of structure
        % return the field as a cell array.
        fnm=fieldnames(StrArr);
        ind_fnm = find(strcmp(fnm(:),FieldName));
        CellArr = struct2cell(StrArr);
        FieldVals = CellArr(ind_fnm,:)';
    end
    
    function out = findInStrArr(StrArr, CtrlF, CtrlV, varargin)
           % argument (mendatory)
           % StrArr     - the structure array to be searched
           % CtrlF      - control field which value to check (char/string)
           % CtrlV      - control value (char/string/numeric)
           % (optional)
           % TrgF      - target field which value to get back (char/string)
           
           %distribution the inputs:
           if nargin==0+3
               flag=0;
           elseif nargin==1+3
               flag=1;
               TrgF = varargin{1};
           else
               flag=-1;
           end
           
           % switching depending on what is tasked (depends on inputs)
           switch flag
               case -1
                   warning('Too many input arguments!')
                   out=[];
               case 0
                   % get the index of the matching Ctrl Field/value
                   Trg_ind = findin(StrArr,CtrlF,CtrlV);
                   out = Trg_ind;
               case 1
                   %get the array index on the target
                   Trg_ind = findin(StrArr,CtrlF,CtrlV);
                   %get the value on the target field
                   TrgV = [StrArr(Trg_ind).(TrgF)];
                   out = TrgV;
           end

           %%
           % ancillary
           function match_ind=findin(StrArr,CtrlF,CtrlV)
                %simple version of thecaller function, returns the index only
                % cell of field names
                fname=fieldnames(StrArr);
                % map strarr to cellarr:
                CellArr=struct2cell(StrArr);
                % get the cellarray index of CtrlF
                cell_ind=find(strcmp(fname,CtrlF));
                % get the index of the CtrlV in the cellarray
                if ischar(CtrlV) || isstring(CtrlV)
                    match_ind = find(strcmp(CellArr(cell_ind,:),CtrlV));
                elseif isnumeric(CtrlV)
                    match_ind = find(CellArr(cell_ind,:)==CtrlV);
                end
           end
%%
    end

    function out = filterStrArr(StrArr,varargin)
        % create filtered Structure array based on conditions on fields
        % fields can be any, multiple conditions ok
        % arguments:
        % StrArr    structure array of the mesa, with fields see in "summary_allcraters"
        
        %parse variable arguments
        try
            varargin = reshape(varargin,[2,numel(varargin)/2])';
        catch
            error('Number of arguments should be an even number (field/val pairs)')
        end
        
        % for the date, we imagine either filtering for a particuliar date
        % or in between date:
        date_ind=find(~cellfun(@isempty,strfind(varargin(:,1),'date')));
        if numel(date_ind)==2
            % order the dates
            if varargin{date_ind(1),2}>varargin{date_ind(1),2}
                varargin(date_ind,2)=varargin(fliplr(date_ind),2);
            end
            dateinterv=1;
            xnumdate=1; % there is one "extra" date, but only generate one filtering column in the filtering matrix
        elseif numel(date_ind)>2
            warning('Date should be a single vale or an interval!')
            dateinterv=0;
            xnumdate=0; % there is many "extra" date, but still generate one filtering column in the filtering matrix
        elseif numel(date_ind)==1
            % is it an array of two?
            if numel(varargin{date_ind,2})>1
                %we will just emulate an extended varargin with the two
                %first items:
                valdate=varargin{date_ind,2} % the date array
                em_date_ind = size(varargin,1)+1; % new index (continuing varargin)
                varargin{em_date_ind,1}='date'; % create extra field
                varargin{date_ind,1} = valdate(1); 
                varargin{em_date_ind,2} = valdate(2);
                % "new" varargin done
                if varargin{date_ind(1),2}>varargin{date_ind(1),2}
                    varargin(date_ind,2)=varargin(fliplr(date_ind),2);
                end
                dateinterv=1;
                xnumdate=1; % there is one "extra" date, but only generate one filtering column in the filtering matrix
            else
                dateinterv=0;
                xnumdate=0; % regular one date case
            end
                
        else
                dateinterv=0; %not really necessary
                xnumdate=0; % regular one date case
                
        end
        
        % we will use Extractfield function, so we can wrap it locally
        % around the StrArr, as a function of the varargin index of the searched field:
        XtrArr = @(argind) ExtractField(StrArr, varargin{argind,1});
        
        %loop around the number of filtering criteria:
        num_crit=size(varargin,1);
        % initialize the filtering bit matrix
        filtmat=zeros(numel(StrArr),num_crit-xnumdate);
        %fill the matrix for each criterion

        for k=1:num_crit
           sort=@(Str)  strcmp(varargin{k,1},Str); % wraps the string comparison to save space
           if sort('name') || sort('mesa') || sort('folder')
               filtmat(:,k) = ~cellfun(@isempty,strfind(XtrArr(k),varargin{k,2}));
           elseif sort('date')
               datearray=cellfun(@datetime, XtrArr(k));
               if dateinterv==0
                   %matching day:
                   filtmat(:,k) = hours(dateshift(datearray,'start','day')-datetime(varargin{k,2}))==0;
                   
               elseif dateinterv==1
                   %select the date interval
                   upstreamdates = hours(dateshift(datearray,'start','day')-datetime(varargin{k,2}))>=0;
                   % directly get the second date for the interval:
                   
                   downstreamdates = hours(dateshift(datearray,'start','day')-datetime(varargin{date_ind(2),2}))<=0;
                   % bit operation on both cutoffs:
                   filtmat(:,k) = bitand(upstreamdates,downstreamdates);
                   %now make sure the second date field is omitted when
                   %encountered further in the loop:
                   dateinterv=-1;
               else
                   continue % second date encountered, we just ignore, the filtering was already done
               end
           else
               %all other fields can be treated as numeric (even booleans).
               % They have either one value or a lower/upper bound
               filterval = varargin{k,2};
               
               if ~(numel(filterval)==2)
                   if numel(filterval)>2 % in case of too long argument values array:
                       warning('too many bounds for the ', num2str(k),...
                               'th filtering argument (', varargin{k,1},')! First one selected!')
                       filterval=filterval(1);
                   end
                   filtmat(:,k) = cell2mat(XtrArr(k))==filterval;
               else 
                   if filterval(1)>filterval(1)
                       filterval([1,2])=filterval([2,1]); %make sure first value is lowest
                   end
                   filtmat(:,k) = bitand(cell2mat(XtrArr(k))>=filterval(1), cell2mat(XtrArr(k))<=filterval(2));
               end
           end
        end
        
        % by now the matrix should be filled, we can bitand' it:
        final_mask = ones(size(StrArr));
        for j=1:num_crit
           final_mask=bitand(final_mask,filtmat(:,j)); % each step will reduced the number of true bits
        end
        final_mask=logical(final_mask);
        %...aaaand return the array
        out = StrArr(final_mask);

    end
    
    
    
    
    
    
    
    
    
%end