function masked_z=findround(z)
    
    masked_z=z;
    masked_z(z~=0)=1;
    masked_z=bwareaopen(masked_z,10); %remove smaller than 4 pixels areas
    [B,L]=bwboundaries(masked_z,'noholes'); %find boundaries of mask
    threshold = 0.6;

    % loop over the boundaries
    for k = 1:length(B)

      % obtain (X,Y) boundary coordinates corresponding to label 'k'
      boundary = B{k}(:,:);
      M=poly2mask(boundary(:,2),boundary(:,1),size(masked_z,1),size(masked_z,2));
      M=bwconvhull(M);
      [J,M]=bwboundaries(M,'noholes'); %find boundaries of mask
      boundary = J{1}(:,:);
      
      % compute a simple estimate of the object's perimeter
      delta_sq = diff(boundary).^2;    
      perimeter = sum(sqrt(sum(delta_sq,2)));
      r=perimeter/(2*pi);
      [centers,radii,metric]=imfindcircles(M,[fix(r-0.1*r),fix(r+0.1*r)]);
      M=logical(M);
      
      % allow only round objects to proceed
      if isempty(metric)
        masked_z(M)=0;
      elseif metric<threshold
        masked_z(M)=0;
      else

      end

    end



end