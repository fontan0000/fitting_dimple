function rename_Files(dirName)

%function that renames automatically generated *.vk4 files to our usual
%nomenclature (x1y1,x1y2,etc). Right now, only optimized for 3x3 craters!
%(9 files)

    names = dir(dirName);
    names([names.isdir]) = [];
    fileName = {names.name};
    for ii = 1: length(fileName)  %# Loop over the file names
          [~,name,ext]=fileparts(fileName{ii});
%           if strcmp(ext,'.csv')
          nameparts=textscan(name,'%s%s%s%s%s%s%s','Delimiter','_');
        
%           switch char(nameparts{6})
%               case 'YELLOWCAVITY'
%                   newName=strcat(nameparts{1},'_',nameparts{2},'_',nameparts{3},'_',nameparts{4},'_',nameparts{5},'_BLUECAVITY_',nameparts{7},ext);
%               case 'GREENCAVITY'
%                   newName=strcat(nameparts{1},'_',nameparts{2},'_',nameparts{3},'_',nameparts{4},'_',nameparts{5},'_REDCAVITY_',nameparts{7},ext);
%               case '1'
%                   newName=strcat(nameparts{1},'_x5y1',ext);
%               case '2'
%                   newName=strcat(nameparts{1},'_x4y1',ext);
%               case '3'
%                   newName=strcat(nameparts{1},'_x3y1',ext);
%               case '4'
%                   newName=strcat(nameparts{1},'_x2y1',ext);                  
%               case '5'
%                   newName=strcat(nameparts{1},'_x1y1',ext);     
%               case '6'
%                   newName=strcat(nameparts{1},'_x5y2',ext);
%               case '7'
%                   newName=strcat(nameparts{1},'_x4y2',ext);
%               case '8'
%                   newName=strcat(nameparts{1},'_x3y2',ext);
%               case '9'
%                   newName=strcat(nameparts{1},'_x2y2',ext);                  
%               case '10'
%                   newName=strcat(nameparts{1},'_x1y2',ext);   
%               case '11'
%                   newName=strcat(nameparts{1},'_x5y3',ext);
%               case '12'
%                   newName=strcat(nameparts{1},'_x4y3',ext);
%               case '13'
%                   newName=strcat(nameparts{1},'_x3y3',ext);
%               case '14'
%                   newName=strcat(nameparts{1},'_x2y3',ext);                  
%               case '15'
%                   newName=strcat(nameparts{1},'_x1y3',ext); 
%               case '16'
%                   newName=strcat(nameparts{1},'_x5y4',ext);
%               case '17'
%                   newName=strcat(nameparts{1},'_x4y4',ext);
%               case '18'
%                   newName=strcat(nameparts{1},'_x3y4',ext);
%               case '19'
%                   newName=strcat(nameparts{1},'_x2y4',ext);                  
%               case '20'
%                   newName=strcat(nameparts{1},'_x1y4',ext);  
%               case '21'
%                   newName=strcat(nameparts{1},'_x5y5',ext);
%               case '22'
%                   newName=strcat(nameparts{1},'_x4y5',ext);
%               case '23'
%                   newName=strcat(nameparts{1},'_x3y5',ext);
%               case '24'
%                   newName=strcat(nameparts{1},'_x2y5',ext);                  
%               case '25'
%                   newName=strcat(nameparts{1},'_x1y5',ext);  
          end
          newName=strcat(newName,ext);
          
          f = fullfile(dirName, char(newName));
          g = fullfile(dirName, fileName{ii});
          movefile(g,f);        %# Rename the file
          end

    end

