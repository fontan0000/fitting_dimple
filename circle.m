function circlevec = circle(par)
%par=(x0,y0,R)
th = (0:pi/1000:2*pi)';
xunit = par(3) * cos(th) + par(1);
yunit = par(3) * sin(th) + par(2);
circlevec=[xunit,yunit];
end