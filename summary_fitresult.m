function [flag_out, crater_raw, data_arrayed]=summary_fitresult(varargin)
    %{ import the summary text file output by eval_cratersKeyenceVK1000 and
    %treat/displays the data.
    % arg:
    % dirName: string, name of the directory
    % optinonal, flag for producing a graph, saving it (TRUE)
    % return: flagout:  0 all good
    %                  -1 not displayable as a matrix in a plot
    %                  -2 no txt data found
%%
%parsing arguments
dirName=varargin{1};
print_and_save=true; % default, produce a graph
if nargin>1
    print_and_save=varargin{2};
end

%% looking for the right file
 fileName=getAllFiles(dirName); 
 if ~isempty(fileName)
     cnt_files=1;
     keep_searching=true;
     nofilefound=false;

     while keep_searching
        file=fileName{cnt_files};
        cnt_files=cnt_files+1; % increase counter
        if strcmp('.txt',file(end-3:end))
            fid=fopen(file);
            tline=fgetl(fid);
            foundcratermarker=strfind(tline,'Crater');
            if ~isempty(foundcratermarker)
                % did find something
                keep_searching=false;
            end
            fclose(fid);
        end
        % if we reached the end of the fiel, abort mission
        if cnt_files>numel(fileName)
            keep_searching=false;
            warning('No summary file found in this folder for the craters!!')
            nofilefound=true;
        end
     end
 else
     % the folder is empty!
     warning('Folder is empty!!')
     nofilefound=true;
 end
     
%%
 %% import the data
% import the data:
if ~nofilefound
    try
        data = tdfread(file,'\t');
        cat_read= fieldnames(data);
    catch
        warning('tdfread in summary_result.m unable to load data.')
        flag_out=-2;
        crater_raw=[];
        data_arrayed=[];
        return % break out entirely if it fails
    end
    % populate arrays:
    for i=1:numel(cat_read)
    % parse the various data field:
        if strfind('Crater',cat_read{i})
            crater_raw=getfield(data,cat_read{i});
            %pass name in cellarray of strings:
            crater={};
            for name=1:size(crater_raw,1)
                crater{name}=convertCharsToStrings(crater_raw(name,:));
            end
        elseif findstr('Radius',cat_read{i})
            radii=getfield(data,cat_read{i});
        elseif findstr('Depth',cat_read{i})
            depth=getfield(data,cat_read{i});
        elseif findstr('Width',cat_read{i})
            width=getfield(data,cat_read{i});
        elseif findstr('Asymmetry',cat_read{i})
            asym=getfield(data,cat_read{i});
        elseif findstr('theta',cat_read{i})
            theta=getfield(data,cat_read{i});  
        else
            warning('Unknown category in data field')
        end
    end
    data_arrayed=[radii, depth, width, asym, theta];
    % category, ordered in the same way
    cat_ordered={"Radius (um)","Depth (um)","Width (um)", "Asymmetry", "Angle (deg.)"};
    flag_out=0;
else
    flag_out=-2;
    crater_raw=[];
    data_arrayed=[];
    return % abort mission
end

%%

%%

%%
% plot the things:
%check if regular mesa matrix

if print_and_save
    
    try
        reg_mat= crater_raw(1,1)=='x' && crater_raw(1,3)=='y';
    catch
        reg_mat=false;
    end


    if reg_mat
        %regular xnym notation
        coord=[str2num(crater_raw(:,2)), str2num(crater_raw(:,4))];
        %shifting accordingly:
        coord(:,1)=coord(:,1)+1;
        coord(1,2)=coord(1,2)+1;
        mesa_size=[3,4];
        disparray=nan(2*mesa_size+2);

        %plot the shit:
        figure
        for j=1:5
            subplot(2,3,j)
            disparray(sub2ind(2*mesa_size+2,2*coord(:,2),2*coord(:,1)))=data_arrayed(:,j);
            pcolor(disparray)
            colormap(parula)
            colorbar
            axis image
            shading flat
            set(gca,'xtick',[])
            set(gca,'ytick',[])
            xlabel('x-index')
            ylabel('y-index')
            title(cat_ordered{j})
        end
        set(gcf,'position',1e3*[2.2234,1.3634,1.0016,0.3648]);
        %get the chip name:
        bckslashind=strfind(dirName,'\');
        bckslashind=bckslashind(end); % last backslash
        savefigpath=[dirName,dirName(bckslashind:end),'.png'];
        saveas(gcf,savefigpath)
        flag_out=0;
    else
        warning('Cant display, matrix not square')
        flag_out=-1;
    end    
end      
     
end
 
 %end