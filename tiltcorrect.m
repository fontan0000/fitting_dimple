%%%fitplane

function new_Z=tiltcorrect(z)

                %mask it if there are sharp edges
                %masked_z=fixsharpedges(z,0.01);
                [masked_z,~]=mask_mesa(z);
                minmaskedz=min(masked_z(:));
                Nx=size(masked_z,2); %number of x pixels
                Ny=size(masked_z,1); %number of y pixels
                masked_z(masked_z<0)=NaN;
                
                tiltXYZ=[];
                %downsample to fit plane easier
                for ii=1:10:Ny
                    for jj=1:10:Nx
                        if isnan(masked_z(ii,jj))
                            continue
                        else
                            tiltXYZ=[tiltXYZ; ii jj masked_z(ii,jj)];
                        end
                    end
                end
            
                %fit plane to tiltXYZ coordinates
                [n,~,~] = affine_fit(tiltXYZ); %fit plane to tiltXYZ data to determine x and y angle
                normal=n'; %normal vector of plane
                %normal([1,2])=normal([2,1]);
                %%calculating angle of rotation: we want to keep x and y, and correct z tilt
                artif_normal=[0,0,1]; %artificial normal
                theta=acos(dot(normal,artif_normal)/(norm(normal)*norm(artif_normal))); %angle of rotation
                axis=cross(normal,artif_normal)/norm(cross(normal,artif_normal)); %calculate axis of rotation
                c=cos(theta); s=sin(theta); C=1-c;
                rotMatrix=[axis(1)*axis(1)*C+c, axis(1)*axis(2)*C-axis(3)*s, axis(1)*axis(3)*C+axis(2)*s;...
                           axis(2)*axis(1)*C+axis(3)*s, axis(2)*axis(2)*C+c, axis(2)*axis(3)*C-axis(1)*s;...
                           axis(3)*axis(1)*C-axis(2)*s, axis(3)*axis(2)*C+axis(1)*s, axis(3)*axis(3)*C+c];
                    
                %calculate rotated plane values
                Nx=size(z,2); %number of x pixels
                Ny=size(z,1); %number of y pixels
                new_Z=zeros(Ny,Nx);
                for aa=1:Ny
                    for bb=1:Nx
                        newpoint=rotMatrix*[aa;bb;z(aa,bb)];
                        new_Z(aa,bb)=newpoint(3); %adjust only height (z) value
                    end
                end
                new_Z=new_Z-minmaskedz; %set lower limit to zero
                
end
