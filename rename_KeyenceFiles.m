function rename_KeyenceFiles(dirName)

%function that renames automatically generated *.vk4 files to our usual
%nomenclature (x1y1,x1y2,etc). Right now, only optimized for 3x3 craters!
%(9 files)

    names = dir(dirName);
    names([names.isdir]) = [];
    fileName = {names.name};
    for ii = 1: length(fileName)  %# Loop over the file names
          [~,name,ext]=fileparts(fileName{ii});
          nameparts=textscan(name,'%s%s','Delimiter','_');
        
          switch char(nameparts{4})
              case '1'
                  newName=strcat(nameparts{1},'_',nameparts{2},'_',nameparts{3},'_x3y1',ext);
              case '2'
                  newName=strcat(nameparts{1},'_',nameparts{2},'_',nameparts{3},'_x2y1',ext);
              case '3'
                  newName=strcat(nameparts{1},'_',nameparts{2},'_',nameparts{3},'_x1y1',ext);
              case '4'
                  newName=strcat(nameparts{1},'_',nameparts{2},'_',nameparts{3},'_x3y2',ext);                  
              case '5'
                  newName=strcat(nameparts{1},'_',nameparts{2},'_',nameparts{3},'_x2y2',ext);                  
              case '6'
                  newName=strcat(nameparts{1},'_',nameparts{2},'_',nameparts{3},'_x1y2',ext);                  
              case '7'
                  newName=strcat(nameparts{1},'_',nameparts{2},'_',nameparts{3},'_x3y3',ext);                  
              case '8'
                  newName=strcat(nameparts{1},'_',nameparts{2},'_',nameparts{3},'_x2y3',ext);  
              case '9'
                  newName=strcat(nameparts{1},'_',nameparts{2},'_',nameparts{3},'_x1y3',ext);                  
          end
          
          f = fullfile(dirName, char(newName));
          g = fullfile(dirName, fileName{ii});
          movefile(g,f);        %# Rename the file
    end

end